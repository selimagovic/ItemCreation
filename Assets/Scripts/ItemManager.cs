﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ItemCategory{WEAPON,ARMOR};

public class ItemManager : MonoBehaviour
{
    // Game object of one panel
    [SerializeField]
    [Header ("Place Item Category here")]
    private Transform itemCategory;

    [SerializeField]
    [Header("Place Item Type here")]
	private Transform itemType;

    // On UI we have dropdown category which will change based on the category type
    private Dropdown _categoryDropdown,_typeDropdown;
    public static List <string> _weaponList,_armorList;
   
    
    private void Start()
    {
		_categoryDropdown = itemCategory.GetComponentInChildren<Dropdown>() ;
		_typeDropdown = itemType.GetComponentInChildren<Dropdown>();


        _weaponList = new List<string> { "Heavy", "Light" };
        _armorList = new List <string> { "Head Armor", "Cheast Armor", "Arm Armor", "Leg Armor" };

        if (_categoryDropdown.value == 0)
        {
            _typeDropdown.ClearOptions();
            _typeDropdown.AddOptions(_weaponList);
            _typeDropdown.value = 0;
            _typeDropdown.RefreshShownValue();
        }
    }
    private void Update()
    {
        _categoryDropdown.onValueChanged.AddListener(delegate { DropdownValueChanged(); });
        _categoryDropdown.RefreshShownValue();
    }
    private void DropdownValueChanged( )
    {
        if (_categoryDropdown.value == 0)
        {
            _typeDropdown.ClearOptions();
            _typeDropdown.AddOptions(_weaponList);
            _typeDropdown.value = 0;
            _typeDropdown.RefreshShownValue();
            
        }
        else
        {
            _typeDropdown.ClearOptions();
            _typeDropdown.AddOptions(_armorList);
            _typeDropdown.value = 0;
            _typeDropdown.RefreshShownValue();
        }
    }

}
