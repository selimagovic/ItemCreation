﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Invertory : MonoBehaviour 
{
    [SerializeField] List<Item> items;
	[SerializeField] Transform itemsParent;
	[SerializeField] ItemSlots[] itemSlots;

    public event Action<Item> OnItemRightClickedEvent;

    private void Awake()
    {
        for(int i=0; i< itemSlots.Length; i++)
        {
            itemSlots[i].OnRightClickEvent += OnItemRightClickedEvent;
        }
    }
	private void OnValidate()
	{
		if(itemsParent!=null)
		{
			itemSlots = itemsParent.GetComponentsInChildren <ItemSlots>();
		}
		RefreshInvertory();
	}

	private void RefreshInvertory()
	{
		int i = 0;
        for(;i<items.Count && i<itemSlots.Length;i++)
		{
            itemSlots[i].Item = items[i];
		}

		for (; i < itemSlots.Length; i++)
		{
            itemSlots[i].Item = null;
		}
	}

    public bool AddItem(Item item)
    {
        if (IsFull())
            return false;

        items.Add(item);
        RefreshInvertory();
        return true;
    }
    public bool RemoveItem(Item item)
    {
        if(items.Remove(item))
        {
            RefreshInvertory();
            return true;
        }
        return false;
    }

    public bool IsFull()
    {
        return items.Count >= itemSlots.Length;
    }
}
