﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour 
{
	/// <summary>
	/// Private variables
	/// </summary>
	[SerializeField]
	private GameObject createItemPanel;
	[SerializeField]
	private GameObject invertory;

    private static SceneManager sceneManager;

    public static SceneManager Instance
    {
        get
        {
            if(sceneManager==null)
            {
                sceneManager = GameObject.FindObjectOfType<SceneManager>();
            }
            return sceneManager;
        }
    }
	/*
		Name:	ActivateItemCreation
		Desc:	Activates Item Creation panel in Canvas
	*/
	public void ActivateItemCreation()
	{
		if(createItemPanel.activeSelf == false)
		{
			createItemPanel.SetActive(true);
		}
	}

	/*
		Name:	DeactivateItemCreation
		Desc:	Deactivates Item Creation panel in Canvas
	*/

	public void DeactivateItemCreation()
	{
		if (createItemPanel.activeSelf == true)
		{
			createItemPanel.SetActive(false);
		}
	}

	/*
		Name:	ActivateInvertory
		Desc:	Activates Invertory panel in Canvas
	*/
	public void ActivateInvertory()
	{
		if(invertory.activeSelf == false)
		{
			invertory.SetActive(true);
		}
	}

	/*
		Name:	DeactivateInvertory
		Desc:	Deactivates Invertory panel in Canvas
	*/
	public void DeactivateInvertory()
	{
		if (invertory.activeSelf == true)
		{
			invertory.SetActive(false);
		}
	}
    /// <summary>
    /// Application Exit
    /// </summary>
    public void Exit()
    {
        Application.Quit();
    }
}
