﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EquipmentType
{
    Head_Armor,
    Chest_Armor,
    Arm_Armor,
    Leg_Armor,
    Weapon,
}

[CreateAssetMenu]
public class EquipableItem : Item 
{
    public int ItemValue;
    public bool Disenchantable;
    [Space]
    public EquipmentType EquipmentType;
}
