﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvertoryManager : MonoBehaviour {

    [SerializeField]Invertory invertory;
    [SerializeField]EquipmentPanel EquipmentPanel;

    private void Awake()
    {
        invertory.OnItemRightClickedEvent += EquipFromInvertory;
    }

    private void EquipFromInvertory(Item item)
    {
        if(item is EquipableItem)
        {
            Equip((EquipableItem)item);
        }
    }

    public void Equip(EquipableItem item)
    {
        
        if(invertory.RemoveItem(item))
        {
            EquipableItem previousItem;
            if (EquipmentPanel.AddItem(item, out previousItem))
            {
                if (previousItem != null)
                {
                    invertory.AddItem(previousItem);
                }
            }
            else
            {
                invertory.AddItem(item);
            }
        }
    }
   /* public void Unequip(EquipableItem item)
    {
        if(!invertory.IsFull() && EquipmentPanel.RemoveItem(item))
        {
            invertory.AddItem(Item);
        }
    }*/
}
